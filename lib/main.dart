import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text("Detalhes de Usuário"),
            backgroundColor: Colors.deepOrange,
          ),
          body: MyClipsWidget()),
    );
  }
}

class MyClipsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: double.infinity,
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                height: 300,
                width: 300,
                child: ClipOval(
                  child: Image.network(
                      "https://avatars3.githubusercontent.com/u/12883075?s=460&v=4",
                      fit: BoxFit.cover),
                )),
            SizedBox(height: 30),
            Text(
              "Luis Henrique Gomes Camilo",
              style: TextStyle(
                  color: Colors.grey[650],
                  fontSize: 24,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 6),
            Text("lhenriquegomescamilo@gmail.com",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 18,
                    fontWeight: FontWeight.normal)),
            SizedBox(height: 30),
            Material(
              elevation: 7,
              color: Colors.deepOrange,
              borderRadius: BorderRadius.circular(30),
              child: InkWell(
                  borderRadius: BorderRadius.circular(30),
                  onTap: () {},
                  child: Container(
                      width: 300,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30)),
                      padding:
                          EdgeInsets.symmetric(horizontal: 70, vertical: 10),
                      child: Text("Sair",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold)))),
            )
          ],
        ));
  }
}
